'''	DID
pr_hV() begun
	YET TO DO
should_get_path()
should_get_min()
D> case two child root
D> case two child leafs even & odd
D> case two child replaser leaf
D> case two child replaser has child
advanced functionality: merge split find_K enumerate_range
~advanced tests?
'''

class Node( object ) :
	def __init__( self, key, left=None, right=None ) :
		self.val = key
		self.less = left
		self.more = right

	def g_less( self ) :
		return self.less

	def g_more( self ) :
		return self.more

	def change_less( self, another ) :
		self.less = another

	def change_more( self, another ) :
		self.more = another

	def g_val( self ) :
		return self.val

	def change_val( self, new_key ) :
		self.val = new_key

	def am_leaf( self ) :
		return self.less is None and self.more is None

	def less_is_empty( self ) :
		return self.less is None

	def more_is_empty( self ) :
		return self.more is None

	def __eq__( self, another ) :
		return self.val == another.val

	def __gt__( self, another ) :
		return self.val > another.val

	def __lt__( self, another ) : # strictly only need one of these
		return self.val < another.val

	'''def __str__( self ) :
		return "V-" + str( self.val ) + " less of " + str( type(self.less) ) + \
			" more ov " + str( type(self.more) )'''

class Bst( object ) :

	def __init__( self, first=None ) :
		self.root = None
		if first is None :
			return
		elif isinstance( first, list ) :
			self._add_multiple( first )
		else :
			self.add( first )

	def add( self, wanted ) :
		if isinstance( wanted, list ) :
			self._add_multiple( wanted )
		elif self.is_empty() :
			self.root = Node( wanted )
		else :
			insertion = Node( wanted )
			self._add_leaf( self.root, insertion )

	def _add_leaf( self, comparison, new_node ) : 
		'currently, not checking until I enter'
		if comparison == new_node :
			return # not recording duplicates today
		elif comparison > new_node :
			if comparison.less_is_empty( ) :
				comparison.change_less( new_node )
			else :
				self._add_leaf( comparison.g_less( ), new_node )
		else :
			if comparison.more_is_empty( ) :
				comparison.change_more( new_node )
			else :
				self._add_leaf( comparison.g_more( ), new_node )

	def _add_multiple( self, wanted_vals ) :
		# consider checking if sorted?
		for val in wanted_vals :
			self.add( val )
	
	def confirm( self, desire ) :
		if self.is_empty() :
			return False
		elif self.root.g_val() == desire :
			return True
		else :
			fake = Node( desire )
			result = self._find( self.root, fake )
			if result is None :
				return False
			else : # found
				return True

	def _find( self, maybe, unconfirmed ) :
		'maybe already tested, which do I test next?'
		if maybe < unconfirmed :
			maybe = maybe.g_more()
			if maybe is None :
				return maybe
			elif maybe == unconfirmed :
				return maybe # success
			else :
				return self._find( maybe, unconfirmed )
		else : # maybe.is_more()
			maybe = maybe.g_less()
			if maybe == unconfirmed or maybe is None :
				return maybe
			else :
				return self._find( maybe, unconfirmed )

	def delete( self, unwanted_val ) :
		if self.is_empty() :
			return
		elif self.root.g_val() == unwanted_val :
			self._delete_root()
		else :
			obsolete = Node( unwanted_val )
			ancestor_stack = [ ]
			ancestor_stack = self._path_to_obsolete( obsolete, self.root, ancestor_stack )
			if ancestor_stack[ -1 ] is None : # last
				return # obsolete not in this tree
			else :
				self._perform_delete( ancestor_stack )
		
	def _path_to_obsolete( self, obsolete, focus, ancestor_stack ) :
		ancestor_stack.append( focus )
		if focus is None or focus == obsolete :
			return ancestor_stack
		else :
			if focus < obsolete :
				return self._path_to_obsolete( obsolete, focus.g_more( ), ancestor_stack )
			else :
				return self._path_to_obsolete( obsolete, focus.g_less( ), ancestor_stack )

	def _perform_delete( self, ancestor_stack ) :
		o_less_Leaf = True
		obsolete = ancestor_stack.pop( )
		parent_of_o = ancestor_stack.pop( )
		o_was_less = obsolete < parent_of_o
		#
		if obsolete.am_leaf() :
			self._update_parent( parent_of_o, None, o_was_less )
		elif obsolete.less_is_empty() :
			self._replace_with_solo( parent_of_o, o_was_less, obsolete, o_less_Leaf )
		elif obsolete.more_is_empty() :
			self._replace_with_solo( parent_of_o, o_was_less, obsolete, not o_less_Leaf )
		else : # two children
			self._replace_with_two_children( parent_of_o, o_was_less, obsolete )

	def _delete_root( self ) :
		fake = Node( self.root.g_val() + self.root.g_val() ) # fake value is irrelevant
		fake_stack = [fake, self.root]
		self._perform_delete( fake_stack )
		if fake.less_is_empty() :
			self.root = fake.g_more()
		else :
			self.root = fake.g_less()	

	def _update_parent( self, parent, replacer, from_less ) :
		if from_less :
			parent.change_less( replacer )
		else :
			parent.change_more( replacer )

	def _replace_with_solo( self, parent_of_o, from_less, obsolete, o_less_Leaf ) :
		if o_less_Leaf :
			self._update_parent( parent_of_o, obsolete.g_more(), from_less )
		else : # more is empty
			self._update_parent( parent_of_o, obsolete.g_less(), from_less )

	def _replace_with_two_children( self, parent, from_less, obsolete ) :
		left_replace = self._choose_which_replaces( obsolete )
		if left_replace :
			replacer, reps_parent = self._max_and_parent( obsolete.g_less(), obsolete )
			self._less_replace_2_kid( parent, from_less, obsolete, replacer, reps_parent )
		else : # right_replace
			replacer, reps_parent = self._min_and_parent( obsolete.g_more( ), obsolete )
			self._more_replace_2_kid( parent, from_less, obsolete, replacer, reps_parent )

	def _choose_which_replaces( self, obsolete ) :
		if isinstance( obsolete.g_val(), str ) :
			return int( obsolete.g_val(), 36 ) % 2 == 0 # is cast to base 36
		else :
			return obsolete.g_val() % 2 == 0 #  even? fragile with non literal

	def _less_replace_2_kid( self, parent, from_less, obsolete, replacer, reps_parent ) :
		if reps_parent is obsolete :
			replacer.change_more( obsolete.g_more() )
			o_less_Leaf = True
			self._replace_with_solo( parent, from_less, obsolete, not o_less_Leaf )
		else :
			self._update_parent( parent, replacer, from_less )
			replacer.change_more( obsolete.g_more() )
			reps_parent.change_more( replacer.g_less() )
			replacer.change_less( obsolete.g_less() )

	def _more_replace_2_kid( self, parent, from_less, obsolete, replacer, reps_parent ) :
		if reps_parent is obsolete :
			replacer.change_less( obsolete.g_less() )
			o_less_Leaf = True
			self._replace_with_solo( parent, from_less, obsolete, o_less_Leaf )
		else :
			self._update_parent( parent, replacer, from_less )
			replacer.change_less( obsolete.g_less() )
			reps_parent.change_less( replacer.g_more() ) # is this right?
			replacer.change_more( obsolete.g_more() )

	def _max_and_parent( self, a_child, a_root ) :
		if a_child.more_is_empty() :
			return a_child, a_root
		else :
			return self._max_and_parent( a_child.g_more(), a_child )

	def _min_and_parent( self, a_child, a_root ) :
		if a_child.less_is_empty() :
			return a_child, a_root
		# else :
		while not a_child.less_is_empty() :
			a_root = a_child
			a_child = a_child.g_less()
		return a_child, a_root

	def g_all_inorder( self ) :
		everything = []
		if self.root is None :
			return everything
		else :
			everything = self._traverse_in( self.root, everything )
			return everything

	def _traverse_in( self, focus, container ) :
		'in order, obviously'
		if focus is None :
			return container
		else :
			container = self._traverse_in( focus.g_less(), container )
			container.append( focus.g_val() )
			container = self._traverse_in( focus.g_more(), container )
			return container

	def g_all_preorder( self ) :
		'recommended for saving the tree in same configuration'
		everything = []
		if self.root is None :
			return everything
		else :
			everything = self._traverse_pre( self.root, everything )
			return everything

	def _traverse_pre( self, focus, container ) :
		'pre order'
		if focus is None :
			return container
		else :
			container.append( focus.g_val() )
			container = self._traverse_pre( focus.g_less(), container )
			container = self._traverse_pre( focus.g_more(), container )
			return container

	def is_empty( self ) :
		return self.root is None

	def erase( self ) :
		self.root = None

	def x():
		'''
		below is raw tree, by depth
		handcrafted has tidy tree, which uses a stack, (& direction keeping structs?) to vertically show links
		right angle tree, which also uses a stack but more simply than tidy
		grail is top down -horizontal- tree with links; would bottom up work better? um no.
		> I could do depth first and store levels as separate lists, then print at leisure
		'''
		pass

	## this is the old version. new version sends path to determine printing
	def print_tree( self ) :
		if self.is_empty() :
			print "empty tree"
		else :
			print '\' ^ ^ ^'
			self._print_branches( self.root, 0 )
			print '\' v v v'

	def _print_branches( self, node, depth ) :
		if node is None :
			return
		elif node.am_leaf( ) :
			self._print_node( node, depth )
		else :
			self._print_branches( node.g_more(), depth + 1 )
			self._print_node( node, depth )
			self._print_branches( node.g_less(), depth + 1 )

	def _print_node( self, node, tabs ) :
		while tabs > 0 :
			print "-",
			tabs -= 1
		print node.g_val()

	def pr_hV( self ) :
		if tree.is_empty() :
			print ">--"
		else :
			print "v"
			self._pr_hV( [ self.root ] )

	def _pr_hV( self, pr_stack ) :
		vert = pr_stack.__len__() - 1
		if vert >= 0 :
			self._pr_hV_links( pr_stack, vert + 1 )
			print
			self._pr_hV_links( pr_stack, vert )
			pr_stack = self._pr_hV_row( pr_stack )
			self._pr_hV( pr_stack )

	def _pr_hV_links( self, pr_stack, vert ) : # test this addition
		for ind in range( 0, vert ) :
			nn = pr_stack[ ind ]
			if nn is None :
				print "  ",
			else :
				print "| ",

	def _pr_hV_row( self, pr_stack ) :
		## ugh, python print \n suppression substitutes a space
		row_str = []
		focus = pr_stack.pop()
		while not focus is None :
			row_str.append( str( focus.g_val() ) )
			if not focus.less_is_empty() :
				pr_stack.append( focus.g_less() )
			elif not focus.more_is_empty() :
				pr_stack.append( None ),		
			if not focus.more_is_empty() :
				row_str.append( "-" )
			focus = focus.g_more()
		row_str = ''.join( row_str ) # :{ have to turn list to string.
		print row_str
		#self._pr_node_stack( pr_stack )
		pr_stack = self._strip_placeholders( pr_stack )
		return pr_stack

	def _strip_placeholders( self, pr_stack ) :
		'ie None'
		focus = None
		while pr_stack.__len__() > 0 :
			focus = pr_stack.pop()
			if not focus is None :
				pr_stack.append( focus )
				break
		return pr_stack

	def advanced( self ) :
		# merge via change each to a LL, merge like the sort, change back to single bst
		# as described by http://dzmitryhuba.blogspot.com/2011/03/merge-binary-search-trees-in-place.html
		# find Kth, use the stack to record down and check each, sadly. note augmentations to improve
		# enumerate within range ; (prune within range? }:D )
		# split tree; ie all above a val X
		print "A> advanced functionality is a stub"

def node_should_work() :
	flag = node_should_compare()
	return node_should_check_leaves() or flag

def node_should_compare() :
	greater = Node( 5 )
	lesser = Node( 1 )
	if greater < lesser :
		print ( "N>a node of " + greater.g_val() + " < " + lesser.g_val() )
		return something_wrong
	elif greater == lesser :
		print ( "N>a node of " + greater.g_val() + " = " + lesser.g_val() )
		return something_wrong
	else :
		return seems_okay

def node_should_check_leaves() :
	leaf = Node( 5 )
	if not leaf.am_leaf() :
		print "N> new node is not a leaf"
		return something_wrong
	child = Node( 6 )
	leaf.change_more( child )
	if not leaf.less_is_empty() :
		print "N> change_more() inserted into less"
		return something_wrong
	elif leaf.more_is_empty() :
		print "N> didn't insert"
		return something_wrong
	leaf.change_less( leaf.g_more() )
	leaf.change_more( None )
	if not leaf.more_is_empty() :
		print "N> change_more() didn't null when asked"
		return something_wrong
	elif leaf.less_is_empty() :
		print "N> didn't insert child into less from more"
		return something_wrong

def should_erase() :
	tree.root = None
	if not tree.is_empty() :
		print "E> didn't think null root is empty"
		return something_wrong
	tree.root = Node( [] )
	if tree.is_empty() :
		print "E> thinks root with val isn't empty"
		return something_wrong
	tree.erase()
	if not tree.is_empty() :
		print "E> didn't empty the tree"
		return something_wrong
	return something_wrong

def should_insert( tree ) :
	flag = should_add_to_empty( tree )
	flag = should_add_to_root_leaf( tree ) or flag
	flag = should_add_multiple( tree ) or flag
	flag = should_add_to_child( tree ) or flag
	flag = should_not_add_copy( tree ) or flag
	return flag

def should_add_to_empty( tree ) :
	if not tree.is_empty( ) :
		tree.erase( )
	root_v = 'aa'
	tree.add( root_v )
	if not tree.root.g_val() == root_v :
		print "I> didn't add val to empty tree"
		return something_wrong
	tree.erase( )
	return seems_okay

def should_add_to_root_leaf( tree ) :
	root_v = 'aa'
	new_val = 'gg'
	tree.add( root_v )
	tree.add( new_val )
	if tree.root.am_leaf( ) :
		print "I> didn't add val to a node"
		return something_wrong
	elif tree.root.g_more( ).g_val() != new_val :
		print "I> didn't add new to correct side"
		if not tree.root.less_is_empty( ) :
			print "I> -- it added to less for some reason"
		return something_wrong
	return seems_okay

def should_add_to_child( tree ) : # cute opportunity within
	tree.erase()
	root_v = 12 # change these to tuples
	root_l_child = 1
	l_child_of_child = -12
	tree.add( root_v )
	tree.add( root_l_child )
	tree.add( l_child_of_child )
	l_l_grandchild_val = tree.root.g_less().g_less().g_val()
	if not l_child_of_child == l_l_grandchild_val :
		print "I> didn't add less val to less child of root"
		return something_wrong
	return seems_okay

def should_add_multiple( tree ) :
	#print "I> doesn't test multiple yet"
	tree.erase()
	root_val = .5
	l_child = .25
	lis = [root_val, l_child]
	tree.add(lis)
	if not tree.root.g_val() == root_val :
		print "I> multiple didn't deposit first val"
		return something_wrong
	elif not tree.root.g_less().g_val() == l_child :
		print "I> multiple didn't add second val"
		return something_wrong
	return seems_okay

def should_not_add_copy( tree ) :
	tree.erase()
	root_v = 'do'
	child = 're'
	tree.add( root_v )
	tree.add( root_v )
	if not tree.root.am_leaf() :
		print "I> added a second copy of a val"
		return something_wrong
	tree.add( child )
	tree.add( child )
	if not tree.root.g_more().am_leaf() :
		print "I> added redundant node to interior"
		return something_wrong
	tree.add( root_v )
	root_okay = tree.root.less_is_empty()
	child_okay = tree.root.g_more().am_leaf()
	if not root_okay or not child_okay :
		print "I> added redundant on second try"
		return something_wrong
	return seems_okay

def should_confirm( tree ) :
	tree.erase()
	root_v = 4
	val_yes = 9
	val_no = val_yes + 1
	bunch = [ root_v, 3, 6, val_yes]
	if tree.confirm( val_no ) :
		print "C> confirmed value when tree empty"
		return something_wrong
	tree.add( bunch )
	if not tree.confirm( root_v ) :
		print "C> didn't confirm root's value"
		return something_wrong
	elif not tree.confirm( val_yes ) :
		print "C> didn't find key in child node"
		return something_wrong
	elif tree.confirm( val_no ) :
		print "C> tree thinks it contains an uninserted value"
		return something_wrong
	else :
		return seems_okay

def should_delete( tree ) :
	flag = test_deletion_cases( tree )
	flag = test_deletion_utilities( tree ) or flag
	return flag

def test_deletion_cases( tree ) :
	flag = should_D_leaf_root( tree )
	flag = should_D_only_child( tree ) or flag
	flag = should_D_replace_w_sole( tree ) or flag
	flag = should_D_with_2( tree ) or flag
	return flag

def should_D_leaf_root( tree ) :
	tree.erase()
	val = 5
	tree.add( val )
	tree.delete( val )
	if not tree.is_empty() :
		print "D> solitary root didn't delete"
		return something_wrong
	else :
		return seems_okay

def should_D_only_child( tree ) :
	root_v = 4
	l_ch = 3
	m_ch = 5
	tree.add( root_v )
	tree.add( l_ch )
	tree.delete( l_ch )
	if not tree.root.am_leaf() :
		print "D> didn't cut solitary less child"
		return something_wrong
	tree.add( m_ch )
	tree.delete( m_ch )
	if not tree.root.am_leaf() :
		print "D> didn't cut solitary more child"
		return something_wrong
	tree.erase()
	return seems_okay

def should_D_replace_w_sole( tree ) :
	tree.erase()
	mid_v = 'm'
	l_ch = 'd'
	m_ch = 'r'
	tree.add( [ mid_v, l_ch ] )
	tree.delete( mid_v )
	# from root
	if not tree.root.g_val() == l_ch :
		print "D> root not replaced by sole (less) child"
		return something_wrong
	# from interior
	tree.add( [ mid_v, m_ch ] )
	tree.delete( mid_v )
	if not tree.root.g_more().g_val() == m_ch :
		print "D> didn't promote deleted's child to my child"
		return something_wrong
	else :
		return seems_okay

def should_D_with_2( tree ) :
	flag = should_D_2_lev_1( tree )
	flag = should_D_2_lev_2( tree ) or flag
	flag = should_D_2_lev_3( tree ) or flag
	flag = should_D_2_lev_4( tree ) or flag
	return flag

def should_D_2_lev_1( tree ) :
	tree.erase()
	root_v = 'f' # 15
	l_ch = 'c' # 12
	m_ch = 'k' # 20
	next_m = 'z'
	tree.add( [ root_v, l_ch, m_ch ] )
	tree.delete( root_v )
	# odd val root: more replaces
	if not tree.root.g_val() == m_ch :
		print "D> didn't promote more sibling (leaf) to root"
		return something_wrong
	tree.add( next_m )
	tree.delete( m_ch )
	if not tree.root.g_val() == l_ch :
		print "D> didn't promote less sibling (leaf) to root"
		return something_wrong
	else :
		return seems_okay

def should_D_2_lev_2( tree ) :
	'internal version of above'
	tree.erase()
	root_v = 3.0
	l_ch = 2.0
	ll_leaf = 1.0
	lm_leaf = 2.5
	tree.add( [ root_v, l_ch, lm_leaf, ll_leaf ] )
	tree.delete( l_ch )
	if not tree.root.g_less().g_val() == ll_leaf :
		print "D> D2 lev2, less child didn't replace"
		return something_wrong
	ll_leaf_2 = -0.5
	tree.add( ll_leaf_2 )
	tree.delete( ll_leaf )
	if not tree.root.g_less().g_val() == lm_leaf :
		print "D> D2-lev2, more child didn't replace"
		return something_wrong
	else :
		return seems_okay

def should_D_2_lev_3( tree ) :
	flag = sh_D_2_lev_3_ch_replaces( tree )
	flag = sh_D_2_lev_3_sub_replaces( tree ) or flag
	return flag

def sh_D_2_lev_3_ch_replaces( tree ) :
	tree.erase()
	top = 40
	targ = 81
	l_ch = 66
	m_ch = 100 # replaces targ
	m_leaf = 303 # replaces m_ch
	tree.add( [ top, targ, l_ch, m_ch, m_leaf ] )
	tree.delete( targ )
	if not tree.root.g_more().g_val() == m_ch :
		print "D> 2,lev3-C more ch didn't replace"
		return something_wrong
	elif not tree.root.g_more().g_more().g_val() == m_leaf :
		print "D> 2,lev3-C m_leaf didn't assume replacer"
		return something_wrong
	else :
		return seems_okay

def sh_D_2_lev_3_sub_replaces( tree ) :
	'a min/max node will replace obsolete, rather than one of its children'
	# but it has no subtree
	tree.erase()
	top = -10
	targ = -4
	m_ch = 0
	l_ch = -7
	m_leaf = -6 # replaces targ
	l_leaf = -8
	tree.add( [ top, targ, l_ch, m_ch, m_leaf, l_leaf ] )
	tree.delete( targ )
	targ_node = tree.root.g_more() # for code width convenience
	if not targ_node.g_val() == m_leaf :
		print "D> 2,lev3-S m_leaf didn't replace targ"
		return something_wrong
	elif not targ_node.g_less().g_val() == l_ch :
		print "D> 2,lev3-S replacer didnt accept former parent as child"
		return something_wrong
	else :
		return seems_okay

def should_D_2_lev_4( tree ) :
	tree.erase()
	top = 'z'
	targ = 'b'
	t_l_ch = 'a'
	t_m_ch = 'm'
	mid_ch = 'g' # replaces targ
	md_m_ch = 'j' # replaces mid_ch
	tree.add( [ top, targ, t_l_ch, t_m_ch, mid_ch, md_m_ch ] )
	tree.delete( targ )
	targ_node = tree.root.g_less()
	if not targ_node.g_val() == mid_ch :
		print "D> D_lev4 obsolete not replaced correctly"
		return something_wrong
	elif not targ_node.g_more().g_val() == t_m_ch :
		print "D> D_lev4 replacer didn't keep obs_more_child, rep's parent"
		return something_wrong
	elif not targ_node.g_more().g_less().g_val() == md_m_ch :
		print "D> D_lev4 didn't promote replacers ch to replacee spot"
		return something_wrong
	else :
		return seems_okay

def test_deletion_utilities( tree ) : # verify
	print "--D> verify deletion utility tests"
	flag = should_get_parent( tree )
	flag = should_get_min_max( tree ) or flag
	return flag

def should_get_parent( tree ) : # verify
	left_val = 3
	root_val = 5
	between_val = 6
	parent_val = 7
	outer_val = 8
	from_left = True
	tree.add( root_val )
	tree.add( left_val )
	tree.add( parent_val )
	tree.add( between_val )
	tree.add( outer_val )
	print "-D> does not Test get_path()"
	return something_wrong

def should_get_min_max( tree ) : # verify
	left_val = 3
	root_val = 5
	between_val = 6
	parent_val = 7
	outer_val = 8
	from_left = True
	tree.add( root_val )
	tree.add( left_val )
	tree.add( parent_val )
	tree.add( between_val )
	tree.add( outer_val )
	print "-D> Doesn't test min / max"
	return something_wrong

def should_traverse( tree ) :
	tree.erase()
	bunch = [ 4, 2, 1, 3, 7, 5, 8 ] # preorder
	tree.add( bunch )
	bundle = tree.g_all_preorder()
	if elem_dont_match( bundle, bunch, "preorder" ) :
		return something_wrong
	bundle = tree.g_all_inorder()
	bunch.sort() # now, inorder
	if elem_dont_match( bundle, bunch, "inorder" ) :
		return something_wrong
	tree.erase()
	return seems_okay

def elem_dont_match( unknown, valid, type ) :
	for elem in range( 0, valid.__len__() - 1 )	:
		if not unknown[ elem ] == valid[ elem ] :
			print "T> " + type + " traversal didn't match expected order"
			return something_wrong
	return seems_okay

def show_adding( tree ) :
	print "W> doesn't show adding"

def show_confirming( tree ) :
	print "W> doesn't show confirming"

def show_deletion( tree ) :
	lis = [5, 3, 1, 2, 4, 8, 7, 9, 10]
	tree._add_multiple( lis )
	tree.print_tree()
	tree.delete( 5 )
	print " + + + *deletes 5*"
	tree.print_tree()
	tree.erase()

def show_traversal( tree ) :
	# assuming it came in with stuff
	if tree.is_empty() :
		tree.add( [ 'f', 'r', 'a', 'b', 'x', 'n', 'm' ] )
	print tree.g_all_inorder()
	print tree.g_all_preorder()

def show_handcrafted_tree( ) :
	print "_+_+_+_+ eventually make"
	#nn = [ "   r-1", "   !", " r-2", " ! i", " ! t-3", " !", \
		#">4", " i", " i r-5", " i !", " t-6", "   i", "   t-7" ]
	nn = [ "   r-1", " r-2", " ! t-3", ">4", " i r-5", " t-6", "   t-7" ]
	for st in nn :
		print st

def check_by_eye( tree ) :
	show_adding( tree )
	show_confirming( tree )
	show_deletion( tree )
	show_traversal( tree )
	show_handcrafted_tree( tree )

def print_styles( tree ) :
	tree.erase()
	tree.add( [ 10, 20, 30, 15, 22, 21, 24, 23 ] )
	tree.print_tree()
	tree.pr_hV()
	#tree.pr_tidy()

def tests( tree ) :
	status = seems_okay # initially
	print "\n\t PROBLEMS"
	status = node_should_work() or status
	status = should_erase()  or status
	status = should_insert( tree ) or status # propagates truth
	status = should_confirm( tree ) or status
	status = should_delete( tree ) or status
	status = should_traverse( tree ) or status
	if status != something_wrong :
		print "> Silent test run"
	else :
		print "\n<> so fix that"

tree = Bst()
something_wrong = True
seems_okay = not something_wrong
#tests( tree )
#check_by_eye( tree )
print_styles( tree )
#advanced( tree )











