
'''
	def __rotate_right(self, node):
		""" Rotate the tree right on node. """
		l = node.left
		if node.parent:
			if node is node.parent.right:
				node.parent.right = l
			else:
				node.parent.left = l
		l.parent = node.parent
		node.left = l.right
		if node.left:
			node.left.parent = node
		l.right = node
		node.parent = l
		if not node.grandparent():
			self.root = node.parent

	def __rotate_left(self, node):
		""" Rotate the tree left on node. """
		r = node.right
		if node.parent:
			if node is node.parent.right:
				node.parent.right = r
			else:
				node.parent.left = r
		r.parent = node.parent
		node.right = r.left
		if node.right:
			node.right.parent = node
		r.left = node
		node.parent = r
		if not node.grandparent():
			self.root = node.parent
'''
#-----
class Node( object ) :

	def __init__( self, key, above, less, more, color = False ) :
		# decided not to use autovalues, let's see if that breaks assumptions
		self.val = key
		self.parent = above
		self.left = less
		self.right = more
		self.red = color

	def equal( self, node2 ) :
		return self.value == node2.value

	def greater_than( self, other_node ) :
		return self.value > other_node.val

	def less_than( self, other_node ) :
		return self.value < other_node.val

	def get_parent( self ) :
		return self.parent

	def get_grandparent( self ) :
		if self.parent.is_leaf() : # I am root
			return self.parent # then test again if // I'd have tested if null, not is_leaf
		return self.parent.parent # not good oo or is that okay since node internal? // faster though
		# might return 'none'

	def get_sibling( self ) :
		am_left_child = self.is_left( )
		if am_left_child == None : # root not a child!
			return self.parent # a leaf
		elif not am_left_child :
			return self.parent.right
		else :
			return self.parent.left

	def is_left( self ) : # be wary of trinary reply
		if self.parent.is_leaf() : # I am root; wanted to eliminate None but can't.
			return None
		elif self.parent.left.is_leaf() :
			return False # no sibling
		else :
			return self.parent.left.value == self.value
	
	def get_uncle( self ) :
		return self.parent.get_sibling()
		
	def is_red( self ) :
		return self.red

	def be_black( self ) :
		self.red = False

	def be_red( self ) :
		self.red = True

	def color_str( self ) : # I'll let client describe me
		if self.red :
			return "R"
		return "b"
	
	def is_leaf( self ) :
		return self.val == None

class RBTree( object ) :

	def __init__( self ) :
		# all leaves will connect to this primal leaf
		self.leaf = Node( self, None, None, None, None, False )
		self.root = self.leaf

	def search_bool( self, want_this ) : # assuming this is what client wants
		return self.search_node( want_this ).is_leaf( )

	def search_node( self, want_this )
		fake = Node( self, want_this, None, None, None, False )
		if self.root.equal( fake )
			return self.root
		elif self.root.is_leaf() :
			return self.leaf
		else :
			return self.find_node( fake, self.root )
		
	def find_node( self, want_this, what_here ) :
		## doesn't account for leaves
		if want_this.greater_than( what_here ) :
			if what_here.right.is_leaf( ) :
				return self.leaf
			else :
				return self.find_node( want_this, what_here.right )
		else :
			if what_here.left.is_leaf( ) :
				return self.leaf
			else :
				return self.find_node( want_this, what_here.left )

	def pivot_left( self, goes_left ) :
'''
	def __left_rotate(self, x):
		if not x.right:
			raise "x.right is nil!"
		y = x.right
		x.right = y.left
		if y.left:
			y.left.parent = x
		y.parent = x.parent
		if not x.parent:
			self.root = y
		else:
			if x == x.parent.left:
				x.parent.left = y
			else:
				x.parent.right = y
		y.left = x
		x.parent = y

	def __right_rotate(self, x):
		if not x.left:
			raise "x.left is nil!"
		y = x.left
		x.left = y.right
		if y.right:
			y.right.parent = x
		y.parent = x.parent
		if not x.parent:
			self.root = y
		else:
			if x == x.parent.left:
				x.parent.left = y
			else:
				x.parent.right = y
		y.right = x
		x.parent = y

'''